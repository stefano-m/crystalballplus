#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2011 - 2013 Stefano Mazzucco <stefano -at- curso.re>
# All rights reserved.
#
# This file is part of Crystal Ball Plus.
#
# Crystal Ball Plus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Crystal Ball Plus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Crystal Ball Plus.  If not, see <http://www.gnu.org/licenses/>.

import wx

class ParamDialog(wx.Dialog):
    """Dialog that asks for configuration parameters.

    """
    def __init__(self, parent, id, title, size, style,
                 minerr_d, minerr_a, checklist, nomatch):
        wx.Dialog.__init__(self, parent, id, title, size=size, style=style)

        self.minerr_d = wx.StaticText(self, label="d-spacing error (%)")
        self.minerr_dctrl = wx.TextCtrl(self, value=minerr_d)

        self.minerr_a = wx.StaticText(self, label="angle error (%)")
        self.minerr_actrl = wx.TextCtrl(self, value=minerr_a)

        self.checklabel = wx.StaticText(self, -1, "Check angle error:")
        self.checklist = checklist
        self.checkradio = wx.RadioBox(self, -1, '', pos=(-1, -1),
                                      choices=self.checklist, name="Checkradio")

        self.nomatchlabel = wx.StaticText(self, -1, "Report non-matching values:")
        self.nomatch = nomatch
        self.nomatchradio = wx.RadioBox(self, -1, '', pos=(-1, -1),
                                        choices=self.nomatch, name="Nomatchradio")

        self.okcancel = self.CreateButtonSizer(wx.OK | wx.CANCEL)

        self.parambox = wx.BoxSizer(wx.VERTICAL)
        self.parambox.Add(self.minerr_d, 0, wx.SHAPED | wx.ALIGN_CENTER)
        self.parambox.Add(self.minerr_dctrl, 0, wx.SHAPED | wx.ALIGN_CENTER)
        self.parambox.Add(self.minerr_a, 0, wx.SHAPED | wx.ALIGN_CENTER)
        self.parambox.Add(self.minerr_actrl, 0, wx.SHAPED | wx.ALIGN_CENTER)
        self.parambox.Add(self.checklabel, 0, wx.SHAPED | wx.ALIGN_CENTER)
        self.parambox.Add(self.checkradio, 0, wx.SHAPED | wx.ALIGN_CENTER)
        self.parambox.Add(self.nomatchlabel, 0, wx.SHAPED | wx.ALIGN_CENTER)
        self.parambox.Add(self.nomatchradio, 0, wx.SHAPED | wx.ALIGN_CENTER)
        self.parambox.Add(self.okcancel, 0, wx.SHAPED | wx.ALIGN_CENTER)
        self.SetSizer(self.parambox)

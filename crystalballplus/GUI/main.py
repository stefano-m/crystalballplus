#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2011 - 2013 Stefano Mazzucco <stefano -at- curso.re>
# All rights reserved.
#
# This file is part of Crystal Ball Plus.
#
# Crystal Ball Plus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Crystal Ball Plus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Crystal Ball Plus.  If not, see <http://www.gnu.org/licenses/>.


import wx
from wx.lib.wordwrap import wordwrap

import os
import sys
import cStringIO
import base64

import crystalballplus.release as release
from crystalballplus import *
from crystalballplus.GUI.panels import *
from crystalballplus.GUI.dialogs import *
from crystalballplus.icon import crystal_icon

class CrystalGUI(wx.Frame):
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title, size=wx.DefaultSize) #(600, 360))

        # self.title = title
        # self.version = release.__version__
        # self.year = release.__year__
        # self.pkgname = release.__package_name__
        # self.author = release.__author__

        self.CreateStatusBar()
        self.startmsg = 'Press Start button when ready'
        self.SetStatusText(self.startmsg)

        # set the custom icon
        iconfile = cStringIO.StringIO(base64.b64decode(crystal_icon))
        logo = wx.ImageFromStream(iconfile)
        logo = wx.BitmapFromImage(logo)
        icon = wx.EmptyIcon()
        icon.CopyFromBitmap(logo)
        self.SetIcon(icon)
        iconfile.close()

        # Menu
        mainmenu = wx.Menu()

        self.minerr_d = '1.0'
        self.minerr_a = '5.0'
        self.checklist = ['any', 'all']
        self.check = 'any'
        self.nomatchlst = ['True', 'False']
        self.nomatch = 'True'

        menuparam = mainmenu.Append(1101, "Configure &Parameters\tCtrl+P",
                                    "Configuration parameters")
        self.Bind(wx.EVT_MENU, self.OnParam, menuparam)

        menuabout = mainmenu.Append(wx.ID_ABOUT,
                                    "&About",
                                    "Information about %s" % \
                                    release.__package_name__)
        self.Bind(wx.EVT_MENU, self.OnAbout, menuabout)

        mainmenu.AppendSeparator()

        menuquit = mainmenu.Append(wx.ID_EXIT,
                                   "Q&uit",
                                   "Quit crystalballplus")
        self.Bind(wx.EVT_MENU, self.OnQuit, menuquit)

        # Menubar
        menubar = wx.MenuBar()
        menubar.Append(mainmenu, "&Main")
        self.SetMenuBar(menubar)
        # Main Panel
        self.mainpanel = wx.Panel(self)
        # Notebook
        self.nb = wx.Notebook(self.mainpanel)
        # Control panel
        self.controlpanel = ControlPanel(self.mainpanel)
        # Report Panels that go inside the Notebook
        self.dpanel = LogPanel(self.nb, -1, 'SAVE')
        self.apanel = LogPanel(self.nb, -1, 'SAVE')

        self.nb.AddPage(self.dpanel, 'd-spacings Report')
        self.nb.AddPage(self.apanel, 'angles Report')

        self.box = wx.BoxSizer(wx.HORIZONTAL)
        self.box.Add(self.controlpanel, 0, wx.EXPAND)
        self.box.Add(self.nb, 0, wx.EXPAND)
        self.mainpanel.SetSizer(self.box)
        self.mainpanel.SetAutoLayout(True)
        self.box.Fit(self)
        self.mainpanel.Layout()
        self.mainpanel.Show(True)

    def OnQuit(self, evt):
        self.Close(True)

    def OnAbout(self, evt):
        info = wx.AboutDialogInfo()
        info.Name = release.__package_name__
        info.Version = release.__release__
        info.Copyright = release.__author__
        info.WebSite = ("https://notabug.org/stefano-m/crystalballplus", "Website")
        info.Description = wordwrap(release.__description__,
                                    350, wx.ClientDC(self.mainpanel))
        info.Developers = [release.__author__, ]
        info.License = wordwrap(release.__license_text__,
                                500, wx.ClientDC(self.mainpanel))
        wx.AboutBox(info)

    def OnParam(self, evt):
        dlg = ParamDialog(self, -1, "Configuration Parameters",
                          (200, 250),
                          wx.DEFAULT_DIALOG_STYLE | wx.STAY_ON_TOP |
                          wx.RESIZE_BORDER,
                          self.minerr_d, self.minerr_a, self.checklist,
                          self.nomatchlst)
        dlg.checkradio.SetSelection(self.checklist.index(self.check))
        dlg.nomatchradio.SetSelection(self.nomatchlst.index(self.nomatch))
        val = dlg.ShowModal()
        isOK =  dlg.GetAffirmativeId()
        if val == isOK:
            self.minerr_d =  dlg.minerr_dctrl.GetValue()
            self.minerr_a = dlg.minerr_actrl.GetValue()
            self.check = self.checklist[dlg.checkradio.GetSelection()]
            self.nomatch = self.nomatchlst[dlg.nomatchradio.GetSelection()]
        self.SetStatusText(self.startmsg)
        dlg.Destroy()


def start_GUI():
    """Start the GUI
    """
    app = wx.App(False)
    title = release.__package_name__ + ' v' + release.__version__
    frame = CrystalGUI(None, title)
    frame.Show(True)
    app.MainLoop()


#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2011 - 2013 Stefano Mazzucco <stefano -at- curso.re>
# All rights reserved.
#
# This file is part of Crystal Ball Plus.
#
# Crystal Ball Plus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Crystal Ball Plus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Crystal Ball Plus.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys

if 'linux' not in sys.platform:
    print
    print('This install script only works with GNU/Linux sytems. Exiting.')
    print
    sys.exit(1)

script = 'crystalballplus'
symlink = script[:]

print
print('Installing %s.' % script)
print

script = os.path.abspath('../bin/' + script)

if not os.path.exists(script):
    print('Could not find %s. Exiting.' % script)
    print
    sys.exit(1)

bindir = os.path.join(os.getenv('HOME'), 'bin')

if not os.path.exists(bindir):
    try:
        os.mkdir(bindir)
    except:
        print('Could not create %s. Exiting.' % bindir)
        print
        sys.exit(1)

os.chmod(script, 0755)
symlink =  os.path.join(bindir, symlink)
os.symlink(script, symlink)

if os.path.isfile(symlink) and os.path.islink(symlink):
    print('Installation completed successfully.')
    print('You might wan to edit your .bashrc file and add')
    print('%s to your PATH variable.' % bindir)
    print
    sys.exit(0)
else:
    print('Installation failed. Exiting.')
    print
    sys.exit(1)

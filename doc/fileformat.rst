..  Copyright (C)  2011 - 2013  Stefano Mazzucco <stefano.mazzucco@gmail.com>.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".

The *Diffractogram* File Format: **.dfg**
==========================================

Crystal Ball Plus makes use of a simple file format to read the data: the *Diffractogram* format with extension **dfg**.

.. NOTE::
   The extension is purely used to distinguish the data files from 
   other text files. Crystal Ball Plus can read any text file, no 
   matter its extension.

A *Diffractogram* file is a simple text file where the keyword elements are preceded by a *special comment* symbol, the *double hash* **##**, and the data are organized in columns.

*Diffractogram* files can be read when creating an instance of the Diffractogram class via the method *read_file*:

.. automethod:: crystalballplus.structures.Diffractogram.read_file
   :noindex:


.. NOTE::
   For the **input** (experimental) files, only the *d-spacing* and    *angle_to_x* columns are required:

   .. code-block:: bash

       ## d-Spacing (A)  angle_to_x (deg)
          2.479          0.00
          2.029          55.14

   For the **reference** files instead, the *space group* and *unit cell* keywords are required (plus, of course, the *hkl* and *d-spacing* columns):

   .. code-block:: bash

      ## structure: Fe2 O3
      ## chem name: Iron (III) Oxide - Gamma
      ## space group: P41212 (92)
      ## crystal system: tetragonal
      ## unit cell: 8.332000 8.332000 25.113000 90.000000 90.000000 90.00000 (A) (deg)

      ##  H  K  L   D (A)    

          1  0  1    7.9081  
          1  0  2    6.9426  
          0  0  4    6.2783  
          1  0  3    5.9054

..  Copyright (C)  2011 - 2013  Stefano Mazzucco <stefano.mazzucco@gmail.com>.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".

Crystal Ball Plus documentation
==================================

:Release: |release|
:Date: |today|

Crystal Ball Plus is an improved version of Crystal Ball. Crystal Ball was developed in the scope of the Cooperative Research Program in Nanoscience and Technology between the University of Maryland (UMD) and the National Institute of Science and Technology (NIST) and its code is thus in the public domain. The main developer continued to work on the code after is appointment with UMD ended and decided to release the updated code under the name of Crystal Ball Plus using the GNU GPL version 3 license.

If you find this software useful, you are kindly asked to acknowledge its creator Dr Stefano Mazzucco, Dr Mihaela Tanase and Dr Renu Sharma, the Center for Nanoscale Science and Technology (CNST) at the National Institute of Standards and Technology (NIST), and the Institute for Research in Electronics and Applied Physics (IREAP) at the University of Maryland.

Contents:

.. toctree::
   :maxdepth: 2

   quickstart
   intro
   install
   fileformat
   reports
   module_doc/index

.. NOTE::
   Even though Crystal Ball Plus should work on GNU/Linux, Microsoft Windows 
   and Apple Mac OS, at the time of the writing of this documentation, 
   Crystal Ball Plus has been tested only on a sytem with the following 
   specifications:

   * GNU/Linux Trisquel 6.0 (based on Ubuntu 12.04 LTS)
   * Linux kernel version 3.2.0
   * Python version 2.7.3
   * NumPy version 1.6.1
   * wxPython version 2.8.12

   The examples given here assume that you have a system compatible with the
   specification given.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


..  Copyright (C)  2011 - 2013  Stefano Mazzucco <stefano.mazzucco@gmail.com>.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".

Installation
=============

The Crystal Ball Plus source code is distributed in a *compressed tarball* file (extension **.tar.gz**) named

.. code-block:: bash

   crystalballplus-VERSION.tar.gz

where *VERSION* is the version number, e.g. 2.2.5a.

To install the software, you must extract the source and run the *setup.py* script.

Within a GNU/Linux terminal:

1. Extract the source:

.. code-block:: bash
      
      $ tar xvzf crystalballplus-VERSION.tar.gz

2. Enter in the directory that has been created:

.. code-block:: bash
      
      $ cd crystalballplus-VERSION

3. Run the *setup.py* script:

.. code-block:: bash
      
      $ python setup.py install

      # if you don't have administrator privileges, install locally with:
    
      $ python setup.py install --user

Now, you can remove the source directory if you want:

.. code-block:: bash

   $ cd ..

   $ rm -rf crystalballplus-VERSION

.. NOTE::
   To run Crystal Ball Plus, the executable ``crystalballplus`` must be in your ``$PATH`` environment variable. When installing with administrator privileges, you should not have to do anything. However, installing as a normal user, you might have to do some modifications to your ``$PATH`` variable.

   When using the ``--user`` option in ``setup.py``, the executable should be installed in ``$HOME/.local/bin``. In case such directory is **not** included in ``$PATH`` (i.e. you cannot run ``crystalballplus``), do:
   
   .. code-block:: bash

      $ export PATH=$PATH:"$HOME/.local/bin"

   To make the change permanent, you should put that line in your ``$HOME/.bashrc`` file.

..  Copyright (C)  2011 - 2013  Stefano Mazzucco <stefano.mazzucco@gmail.com>.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".

Introduction to Crystal Ball Plus
*********************************
Crystal Ball Plus is a small piece of software that has been created to help predicting a crystal structure from its diffraction pattern. It's derived from Crystal Ball, a software developed at the National Institute of Standards and Technology in Gaithersburg, MD, USA. The code of Crystal Ball being in the public domain, the main developer continued to work on the program after his collaboration with NIST ended and decided to release a new version under the name of *Crystal Ball Plus* using the *free software* license GNU GPL version 3.

By using linear algebra and tabulated space groups, Crystal Ball Plus tries to match the experimental values of the *d-spacings* and *angles* measured on one - or more - diffraction patterns to a database of known structures provided by the user.

The Problem: Diffraction Pattern Matching is Hard
=================================================
Most of the times, diffraction pattern matching is done by hand by the scientist. He or she  must *carefully* match the *d-spacings* to a number of possible structures, with a precision commonly around 1%, and then repeat the operation for the *angles* between any two reflections, that usually have to match with a precision better than 5%. Even though a human mind is able to process and compare data in parallel, the fact that many structures may present very similar values for either or both the *d-spacings* and the *angles* make the whole process lenghty, tedious and error-prone.

A Possible Solution: Let the Machine do the Math
=================================================
The intrinsic problem in the diffraction pattern matching lies in the fact that a human can get bored and/or distracted while doing all the calculations by hand. If we let a computer doing the math to obtain a set of possible matches, then we would be able to select the best match - if any - using our knowledge of the system under study. An automated procedure might then save a significant amount of time and improve the quality of the results.

Matching is not Enough: Good Measurements in the First Place
============================================================
Of course, a good matching algorithm does not necessarily guarantee the best results. What is needed in the first place is *good data*: the diffraction patterns should be carefully recorded and measured, and the database should always be up-to-date and reliable.

What About the Future? Improving Crystal Ball Plus
==================================================
Crystal Ball Plus has a lot of potential. Its modular structure - intrinsic in any program written in Python - makes it easily expandable. It is desirable that a module to measure reliably both *d-spacings* and *angles* from diffraction patterns be integrated into the program.

Much more can be done. For example, one could implement a way to download the structure database from a remote repository, to extract Fourier transforms from real space images and treat them as diffraction patterns, or even to extract information from videos instead of still images.

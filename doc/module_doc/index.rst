Module Reference
=================

This sections documents the main modules used by Crystal Ball Plus.

.. toctree::
   :maxdepth: 2
   
   operations
   structures
   compare
   report
   utilities

Basic Cristallography - :samp:`operations` and :samp:`spacegroups`
====================================================================

This section lists the modules where the basic cristallography operations are stored. The ``Diffractogram`` and ``MillerPlane`` classes make use of such functions.

The module ``operations`` contains the standard operations for the direct and reciprocal spaces. The module ``spacegroups`` contains the symmetry operations relative to the crystallographic *space groups*, while ``spacegroups_extra`` contains extra space groups representations.

.. currentmodule:: crystalballplus

.. autosummary::
   :toctree: generated

   operations
   spacegroups
   spacegroups_extra

Functions
----------
.. automodule:: crystalballplus.operations

.. autofunction:: direct_metric_tensor

.. autofunction:: reciprocal_metric_tensor

.. autofunction:: unitcell

.. autofunction:: unitcell_volume

.. autofunction:: nccross

.. autofunction:: ncnorm

.. autofunction:: ncdist

.. autofunction:: theta

.. autofunction:: d_spacing

.. autofunction:: zone_axis

.. autofunction:: plane2mill

.. autofunction:: family

.. autofunction:: switch_space

.. autofunction:: switch_uc

.. autofunction:: cartesian

Functions
----------
.. automodule:: crystalballplus.spacegroups

.. autofunction:: get_spacegroup

.. autofunction:: is_spacegroup_identifier
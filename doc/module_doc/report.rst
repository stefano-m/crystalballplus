The Report Functions - :samp:`report`
=============================================

.. automodule:: crystalballplus.report

Functions
----------

.. autofunction:: gen_angle_report

.. autofunction:: gen_d_report

The *MillerPlane* and *Diffractogram* classes - :samp:`structures`
=====================================================================

The two classes utilized by Crystal Ball Plus are ``MillerPlane`` and ``Diffractogram``. The former defines a generic *Miller Plane* given its indices; the latter defines a generic *Diffraction Pattern*.

The ``Diffractogram`` class is the core of Crystal Ball Plus, in it all the caractheristics of a structure necessary for the indexing are defined.

.. currentmodule:: crystalballplus

.. autosummary::
   :toctree: generated

   structures

Classes
----------
.. automodule:: crystalballplus.structures

.. autoclass:: MillerPlane

.. autoclass:: Diffractogram

..  Copyright (C)  2011 - 2013  Stefano Mazzucco <stefano.mazzucco@gmail.com>.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".

Quickstart
**********

Graphical User Interface
========================
The easiest and fastest way to run Crystal Ball Plus is to launch its simple graphical user interface (GUI). 

In a GNU/Linux terminal, type:

.. code-block:: bash

    $ crystalballplus -GUI

The GUI will pop up:

.. image:: images/crystalballplus_GUI.png
   :alt: Crystal Ball Plus GUI
   :align: left

.. NOTE::
   You will need `wxPython <http://www.wxpython.org/>`_ installed for the GUI to work. Also, the executable ``crystalballplus`` must be in your environment variable ``$PATH`` (see also :doc:`install`).

On the left side of the window, the control panel allows you to select both the input and reference files. These files should be text files with the **dfg** extension (see :doc:`fileformat`).

#. Use the radio buttons to specify whether you are selecting a single **dfg** file (*File*) or all the **dfg** files contained within a given folder (*Folder*)
#. Click on *Select Input* to browse the file or folder
#. The name of the selected file or folder will appear in the textbox in the second row.

Similarly, click on the *Select Reference* button to select the reference files from a reference database.

Now you should click on the *START* button to initiate the procedure.

On the right side of the window there are two *read-only* tabs that will display respectively the d-spacings and angles reports. Click on the *Save* button while visualizing a given report to save it.

A few parameters can be manually adjusted. To do so, press :kbd:`Ctrl+P` or go to :menuselection:`Main --> Configure Parameters` to display the configuration window:

.. image:: images/crystalballplus_params.png
   :alt: Crystal Ball Plus parameters window
   :align: left

The options are:

* the maximum *percent error* within the *d-spacings* and *angles* will be considered to match (defaults respectively to 1% and 5%)
* whether *any* or *all* the angles of the diffraction pattern should be within the given *percent error*
* whether the non-matching structures should be reported (see :doc:`reports`)

You can follow state of the current run on the progress and status bars.

You can quit the program by pressing :kbd:`Ctrl+Q` or by going to :menuselection:`Main --> Quit`.


Text Interface
===============

The text interface is a little bit more complicated, but it is a useful tool if one wants to perform several runs in a batch (e.g. overnight). Also, you will have to run the text interface if wxPython is not installed.

The options are:

.. code-block:: bash

    crystalballplus [-i input_path][-r reference_path]
                    [-d_err d_error][-a_err angle_error]
                    [-check all|any][-o][-d d_report][-a angle_report]


+--------------------+-----------------------------------+
| OPTIONS            |     DESCRIPTION                   |
+====================+===================================+
| -i <input_path>    |     path to input file(s).        |
+--------------------+-----------------------------------+
| -r <ref_path>      |    path to reference file(s).     |
+--------------------+-----------------------------------+
| -d_err <d_error>   |     d-spacing error in %.         |
+--------------------+-----------------------------------+
| -a_err <a_error>   |     angle error in %.             |
+--------------------+-----------------------------------+
| -check <all | any> |     angle check option.           |
+--------------------+-----------------------------------+
| -d <d_report>      |     d-spacing report file.        |
+--------------------+-----------------------------------+
| -a <ang_report>    |     angles report file.           |
+--------------------+-----------------------------------+
| -o                 |     overwrite repots.             |
+--------------------+-----------------------------------+

During the run, useful information will be printed on the standard output.

Help Message
=============
If you are lost, try the help message:

.. code-block:: bash

    $ crystalballplus -h

    Usage:

    crystalballplus [[-GUI][-i input_path][-r reference_path]
                    [-d_err d_error][-a_err angle_error][-check all|any][-o]
                    [-d d_report][-a angle_report][-h]]

    Load the input and reference files (extension: .dfg),
    and save the results in two report files (text format).
    The user will be prompted to insert the parameters that have not
    been specified.

    OPTIONS
    -GUI                    : start the GUI
    -i <input_path>         : path to input file(s).
    -r <reference_path>     : path to reference file(s).
    -d_err <d_error>        : d-spacing error in %.
    -a_err <a_error>        : angle error in %.
    -check <all | any>      : angle check option.
    -d <d_report>           : d-spacing report file.
    -a <angle_report>       : angles report file.
    -o                      : overwrite repots.
    -v                      : print version and exit.
    -h                      : print this message and exit.

# Build Documentation

To build the Crystal Ball Plus documentation, you need the ```python-sphinx``` package and GNU Make.

Change to the ```docs``` directory and run ```make help``` to see what types of documentation you can build:

    doc$ make help
    Please use `make <target>' where <target> is one of
      html      to make standalone HTML files
      dirhtml   to make HTML files named index.html in directories
      pickle    to make pickle files
      json      to make JSON files
      htmlhelp  to make HTML files and a HTML help project
      qthelp    to make HTML files and a qthelp project
      latex     to make LaTeX files, you can set PAPER=a4 or PAPER=letter
      changes   to make an overview of all changed/added/deprecated items
      linkcheck to check all external links for integrity
      doctest   to run all doctests embedded in the documentation (if enabled)

Usually, ```make html``` is the easiest option. You will need to run it at least twice in order to remove any warning and create all the HTML links. After that, you can open the file ```_build/html/index.html``` to start navigating the documentation.

You can also generate the latex documentation and then compile it to obtain a ```dvi```, ```ps``` or ```pdf``` document (you will need extra programs installed as ```latex```, ```dvips``` and ```pdflatex```). Just run ```make latex```, then change to ```_build/latex``` and run respectively ```make all-dvi```, ```make all-ps``` or ```make all-pdf```. Read the generated Makefile for more available options.
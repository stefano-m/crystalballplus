Crystal Ball Plus is an improved version of Crystal Ball. Crystal Ball was developed in the scope of the Cooperative Research Program in Nanoscience and Technology between the University of Maryland (UMD) and the National Institute of Science and Technology (NIST) and its code is thus in the public domain. The main developer continued to work on the code after is appointment with UMD ended and decided to release the updated code under the name of Crystal Ball Plus using the GNU GPL version 3 license.

If you find this software useful, you are kindly asked to acknowledge its creator Dr Stefano Mazzucco, Dr Mihaela Tanase and Dr Renu Sharma, the Center for Nanoscale Science and Technology (CNST) at the National Institute of Standards and Technology (NIST), and the Institute for Research in Electronics and Applied Physics (IREAP) at the University of Maryland.

## Contents:
* [Installation](https://notabug.org/stefano-m/crystalballplus/src/master/doc/wiki/Installation.md)
* [Build Documentation](https://notabug.org/stefano-m/crystalballplus/src/master/doc/wiki/Build%20Documentation.md)
* [Quickstart](https://notabug.org/stefano-m/crystalballplus/src/master/doc/wiki/Quickstart.md)

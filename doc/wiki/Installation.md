# Installation

At the moment, Crystal Ball Plus is not distributed as an installable package by itself. However, you can easily install it from source using Python.

The following procedure should work for any GNU/Linux machine that has the Python dependencies correctly installed.

* Open a terminal.
* Download the source code from notabug ```https://notabug.org/stefano-m/crystalballplus.git```
* Choose whether you want to use the development version or a tag (i.e. release). If you want a tag/release, do ```git tag``` to see what's available and then check out the chosen tag, e.g. ```git checkout v2.2.21a3```.
* Run ```python setup.py install``` for a global installation (this will install to ```/usr/local``` by default an you will need root privileges (e.g. run with ```sudo```).
* Alternatively, run ```python setup.py install --user``` to install locally in ```~/.local```. In this case, you will need to add ```~/.local/bin``` to your ```$PATH``` directory to be able to run the software (you can set this in e.g. ```.bashrc``` or ```.profile``` by adding the line ```export PATH=$PATH:"$HOME/.local/bin"```).

You can as well build a *source distribution*, i.e. a compressed archive that you can easily redistribute. To do so, run ```python setup.py sdist``` and profit of the compressed archive that will be generated in the ```dist``` directory.
# Quickstart

## Graphical User Interface

The easiest and fastest way to run Crystal Ball Plus is to launch its simple graphical user interface (GUI).

In a GNU/Linux terminal, type:

`$ crystalballplus -GUI`

The GUI will pop up:

![image](https://notabug.org/stefano-m/crystalballplus/raw/master/doc/images/crystalballplus_GUI.png"GUI")

>Note:
>You will need wxPython installed for the GUI to work. Also, the executable crystalballplus must be in your environment variable $PATH (see also [Installation](https://notabug.org/stefano-m/crystalballplus/src/master/doc/wiki/Installation.md)).

On the left side of the window, the control panel allows you to select both the input and reference files. These files should be text files with the dfg extension (see The Diffractogram File Format: .dfg).

1. Use the radio buttons to specify whether you are selecting a single dfg file (File) or all the dfg files contained within a given folder (Folder)
2. Click on Select Input to browse the file or folder
3. The name of the selected file or folder will appear in the textbox in the second row.

Similarly, click on the Select Reference button to select the reference files from a reference database.
Now you should click on the `START` button to initiate the procedure.

On the right side of the window there are two read-only tabs that will display respectively the d-spacings and angles reports. Click on the `Save` button while visualizing a given report to save it.

A few parameters can be manually adjusted. To do so, press `Ctrl+P` or go to `Main ‣ Configure Parameters` to display the configuration window:

![image](https://notabug.org/stefano-m/crystalballplus/raw/master/doc/images/crystalballplus_params.png"Crystal Ball Plus parameters window")

The options are:

* the maximum percent error within the d-spacings and angles will be considered to match (defaults respectively to 1% and 5%)
* whether any or all the angles of the diffraction pattern should be within the given percent error
* whether the non-matching structures should be reported (see The Report Files)

You can follow state of the current run on the progress and status bars.

You can quit the program by pressing `Ctrl+Q` or by going to `Main ‣ Quit`.

## Text Interface

The text interface is a little bit more complicated, but it is a useful tool if one wants to perform several runs in a batch (e.g. overnight). Also, you will have to run the text interface if wxPython is not installed.

The options are:

    crystalballplus [-i input_path][-r reference_path]
     [-d_err d_error][-a_err angle_error]
     [-check all|any][-o][-d d_report][-a angle_report]

If you are lost, try the help message:

     $ crystalballplus -h

    Usage:

    crystalballplus [[-GUI][-i input_path][-r reference_path]
                    [-d_err d_error][-a_err angle_error][-check all|any][-o]
                    [-d d_report][-a angle_report][-h]]

    Load the input and reference files (extension: .dfg),
    and save the results in two report files (text format).
    The user will be prompted to insert the parameters that have not
    been specified.

    OPTIONS
    -GUI                    : start the GUI
    -i <input_path>         : path to input file(s).
    -r <reference_path>     : path to reference file(s).
    -d_err <d_error>        : d-spacing error in %.
    -a_err <a_error>        : angle error in %.
    -check <all | any>      : angle check option.
    -d <d_report>           : d-spacing report file.
    -a <angle_report>       : angles report file.
    -o                      : overwrite repots.
    -v                      : print version and exit.
    -h                      : print this message and exit.

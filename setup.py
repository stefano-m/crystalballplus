#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2011 - 2013 Stefano Mazzucco <stefano -at- curso.re>
# All rights reserved.
#
# This file is part of Crystal Ball Plus.
#
# Crystal Ball Plus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Crystal Ball Plus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Crystal Ball Plus.  If not, see <http://www.gnu.org/licenses/>.

from distutils.core import setup

import crystalballplus.release as release

modules = [
    '__init__',
    'structures',
    'compare',
    'operations',
    'report',
    'spacegroups',
    'spacegroups_extra',
    'fileio',
    'release',
    'utilities',
            ]

setup(
    name=str(release.__package_name__),
    version=str(release.__version__),
    license=str(release.__license__),
    description=str(release.__description__),
    long_description=str(release.__description__),
    author=str(release.__author__),
    author_email=str(release.__author_email__),
    url=str(release.__website__),
    download_url=str(release.__website__),
    packages=[str(release.__package_name__), str(release.__package_gui_name__)],
    scripts=['bin/crystalballplus',],
    )
